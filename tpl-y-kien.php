<?php
/**
 * Template Name: Cảm nhận
 */
get_header();
?>
<div class="container">
    <div class="row">
        <main id="main-content" class="main-content col-md-12">
            <h2 class="khoa-hoc-title"><?php the_title(); ?></h2>
            <?php
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            $args = array(
                'post_type' => 'cam-nhan',
                'post_status' => 'publish',
                'posts_per_page' => 6,
                'order' => 'DESC',
                'paged' => $paged
            );
            $my_query = new wp_query($args);
            if($my_query->have_posts()) { ?>
                <ul class="cam-nhan-list">
                    <?php
                    while ($my_query->have_posts()):$my_query->the_post(); ?>
                        <li class="cam-nhan-item clearfix">
                            <div class="row">
                                <div class="col-md-2">
                                    <?php the_post_thumbnail('avatar_thumb_150x150');?>
                                </div>
                                <div class="col-md-10">
                                    <div class="cam-nhan-content">
                                        <?php the_excerpt(); ?>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php endwhile;
                    wp_pagenavi( array( 'query' => $my_query ) );
                    wp_reset_query();
                    ?>
                </ul>
                <?php wp_pagenavi(); ?>
            <?php } ?>
        </main><!-- #main-content -->
    </div>
</div>

<?php get_footer(); ?>
