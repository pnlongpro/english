<?php
get_header();
?>
<div class="container">
	<div class="row">
		<main id="main-content" class="main-content col-md-9">
			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="entry-header">
						<?php vi_theme_entry_header(); ?>
						<?php //vi_theme_entry_meta(); ?>
					</div><!-- .entry-header -->
					<div class="entry-content">
						<?php vi_theme_entry_content(); ?>
					</div><!-- .entry-content -->
					<div class="entry-footer">
						<a href="<?php the_field('file_download', $post->ID); ?>" class="download-file"><?php _e('Tải tài liệu', 'vi_theme'); ?></a>
					</div>
				</article><!-- #post-## -->
			<?php endwhile; // end of the loop. ?>
		</main><!-- #main-content -->
		<div id="sidebar-right" class="sidebar col-md-3">
			<?php if ( ! dynamic_sidebar( 'thu-vien' ) ) : ?>
			<?php endif;?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
