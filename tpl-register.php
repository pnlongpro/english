<?php
/**
 * Template Name: Register Page
 */
get_header();
?>
<div class="container">
    <div class="row">
        <main id="main-content" class="main-content col-md-12">
            <h2 class="khoa-hoc-title"><?php the_title(); ?></h2>
            <?php
            if (is_user_logged_in()) {
                echo '<div class="aa_logout"> Hello, <div class="aa_logout_user">', $user_login, '. You are already logged in.</div><a id="wp-submit" href="', wp_logout_url(), '" title="Logout">Logout</a></div>';
            } else { ?>
                <form name="registerform" action="<?php echo site_url('wp-login.php?action=register', 'login_post') ?>" method="post">
                    <p>
                        <label for="user_login">Username</label>
                        <input type="text" name="user_login" value="">
                    </p>
                    <p>
                        <label for="user_email">E-mail</label>
                        <input type="text" name="user_email" id="user_email" value="">
                    </p>
                    <p style="display:none">
                        <label for="confirm_email">Please leave this field empty</label>
                        <input type="text" name="confirm_email" id="confirm_email" value="">
                    </p>

                    <p id="reg_passmail">A password will be e-mailed to you.</p>

                    <input type="hidden" name="redirect_to" value="/login/?action=register&success=1" />
                    <p class="submit"><input type="submit" name="wp-submit" id="wp-submit" value="Register" /></p>
                </form>
            <?php }
            ?>
        </main><!-- #main-content -->
    </div>
</div>

<?php get_footer(); ?>