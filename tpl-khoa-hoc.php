<?php
/**
 * Template Name: Khóa học
 */
get_header();
the_post();
?>
<div class="container">
    <h2 class="khoa-hoc-title"><?php _e('Khóa học', 'vi_theme'); ?></h2>
    <div class="row">
        <div id="sidebar-left" class="sidebar col-md-3">
            <?php if ( ! dynamic_sidebar( 'khoa-hoc' ) ) : ?>
            <?php endif;?>
        </div>
        <main id="main-content" class="main-content col-md-9">
            <article>
                <div class="entry-header">
                    <h1 class="entry-title">
                        <span><?php echo $term->name; ?></span>
                    </h1>
                    <?php //vi_theme_entry_meta(); ?>
                </div><!-- .entry-header -->
                <div class="entry-content">
                    <?php the_content(); ?>
                </div><!-- .entry-content -->
            </article><!-- #post-## -->
        </main><!-- #main-content -->
    </div>
</div>

<?php get_footer(); ?>