<?php
	$logo = ot_get_option('logo');
	$favicon = ot_get_option('icon');
	$home_slider = ot_get_option('shortcode_home_slider');
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="shortcut icon" href=" <?php echo $favicon; ?>" type="image/x-icon" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="fb-root"></div>
<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5&appId=<?php echo ot_get_option('facebook_app_id'); ?>";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
<div id="page" class="hfeed site">

	<header id="header" class="header">
		<div class="header-top clearfix">
			<div class="container">
				<div class="logo">
					<?php vi_theme_logo(1, $logo); ?>
				</div>
				<div class="main-menu">
					<?php vi_theme_menu('premary'); ?>
				</div>
			</div>
		</div>
		<div class="header-slider">
		<?php
			if(is_home() || is_front_page()) { ?>
				<?php echo do_shortcode($home_slider); ?>
			<?php } else { ?>
				<img src="<?php echo get_template_directory_uri(); ?>/images/slider-1.jpg" alt="">
			<?php }
		?>
		</div>

	</header><!-- #header -->
    <?php if(!(is_home() || is_front_page())) { ?>
        <div class="breadcrumbs">
            <div class="container">
                <?php if(function_exists('bcn_display'))
                {
                    bcn_display();
                }?>
            </div>
        </div>
    <?php } ?>
	<section class="content-area">