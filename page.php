<?php
get_header();
?>
<div class="container">
    <div class="row">
        <div id="sidebar-left" class="sidebar col-md-3">
            <?php if ( ! dynamic_sidebar( 'sidebar-single' ) ) : ?>
            <?php endif;?>
        </div>
        <main id="main-content" class="main-content col-md-9">
            <?php while ( have_posts() ) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <div class="entry-header">
                        <?php vi_theme_entry_header(); ?>
                        <?php //vi_theme_entry_meta(); ?>
                    </div><!-- .entry-header -->
                    <div class="entry-content">
                        <?php vi_theme_entry_content(); ?>
                    </div><!-- .entry-content -->
                    <div class="entry-footer">
                    </div>
                </article><!-- #post-## -->
            <?php endwhile; // end of the loop. ?>
        </main><!-- #main-content -->
    </div>
</div>

<?php get_footer(); ?>
