<?php
	$copyright = ot_get_option('copyright');
?>
</section><!-- .content-area -->

<footer id="footer" class="footer">
	<div class="footer-top">
		<div class="container">
			<div class="row">
				<div class="widget-item col-md-3 col-sm-6 col-xs-6">
					<?php if ( ! dynamic_sidebar( 'footer-block-1' ) ) : ?>
					<?php endif;?>
				</div>
				<div class="widget-item col-md-3 col-sm-6 col-xs-6">
					<?php if ( ! dynamic_sidebar( 'footer-block-2' ) ) : ?>
					<?php endif;?>
				</div>
				<div class="widget-item col-md-3 col-sm-6 col-xs-6">
					<?php if ( ! dynamic_sidebar( 'footer-block-3' ) ) : ?>
					<?php endif;?>
				</div>
				<div class="widget-item col-md-3 col-sm-6 col-xs-6">
					<?php if ( ! dynamic_sidebar( 'footer-block-4' ) ) : ?>
					<?php endif;?>
				</div>
			</div>
		</div>
	</div>

	<?php
		if($copyright) { ?>
            <div class="footer-copy-right">
                <div class="container">
                    <p><?php echo $copyright; ?></p>
                </div>
            </div>
        </div>
        <?php }
	?>
</footer><!-- #colophon -->
</div><!-- #page -->

<a href="#" class="scrolltotop"><i class="fa fa-angle-up"></i></a>
</div><!-- #page -->
<div class="box_overlay"></div>

<?php wp_footer(); ?>

</body>
</html>