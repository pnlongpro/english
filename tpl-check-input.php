<?php
/**
 * Template Name: Test đầu vào
 */
get_header();
the_post();
?>
<div class="container">
    <div class="row">
        <main id="main-content" class="main-content col-md-12">
            <h2 class="khoa-hoc-title"><?php the_title(); ?></h2>

            <div class="row">
                <div class="col-md-6 the_quiz">
                    <?php echo do_shortcode('[WpProQuiz 1]'); ?>
                </div>
                <div class="col-md-6 the_content">
                    <?php the_content(); ?>
                </div>
            </div>

            <?php
            //if (is_user_logged_in()) { ?>

            <?php //} else {
                //wp_login_form($args);
//                $args = array(
//                    'echo'           => true,
//                    'redirect'       => home_url('/wp-admin/'),
//                    'form_id'        => 'loginform',
//                    'label_username' => __( 'Username' ),
//                    'label_password' => __( 'Password' ),
//                    'label_remember' => __( 'Remember Me' ),
//                    'label_log_in'   => __( 'Log In' ),
//                    'id_username'    => 'user_login',
//                    'id_password'    => 'user_pass',
//                    'id_remember'    => 'rememberme',
//                    'id_submit'      => 'wp-submit',
//                    'remember'       => true,
//                    'value_username' => NULL,
//                    'value_remember' => true
//                );
            //}
            ?>
        </main><!-- #main-content -->
    </div>
</div>

<?php get_footer(); ?>
