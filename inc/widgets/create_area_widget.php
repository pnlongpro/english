<?php
function eweb_widgets_init() {
	// Sidebar
	register_sidebar( array(
		'name' => __( 'Sidebar', 'vi_theme' ),
		'id' => 'sidebar',
		'before_widget' => '<div id="%1$s" class="%2$s blog-sidebar">',
		'after_widget' => '</div>',
		'before_title' => '<div class="blog-title"><h3>',
		'after_title' => '</h3></div>'
	));
	// Sidebar Khóa học
	register_sidebar( array(
		'name' => __( 'Sidebar Khóa Học', 'vi_theme' ),
		'id' => 'khoa-hoc',
		'before_widget' => '<div id="%1$s" class="%2$s khoa-hoc-sidebar">',
		'after_widget' => '</div>',
		'before_title' => '<div style="display: none;" class="khoa-hoc-title"><h3>',
		'after_title' => '</h3></div>'
	));
	// Sidebar Thư viện
	register_sidebar( array(
		'name' => __( 'Sidebar Thư viện', 'vi_theme' ),
		'id' => 'thu-vien',
		'before_widget' => '<div id="%1$s" class="%2$s blog-sidebar">',
		'after_widget' => '</div>',
		'before_title' => '<div class="blog-title"><h3>',
		'after_title' => '</h3></div>'
	));

	// Sidebar Single
	register_sidebar( array(
		'name' => __( 'Sidebar Single', 'vi_theme' ),
		'id' => 'sidebar-single',
		'before_widget' => '<div id="%1$s" class="%2$s blog-sidebar">',
		'after_widget' => '</div>',
		'before_title' => '<div class="blog-title"><h3>',
		'after_title' => '</h3></div>'
	));// Sidebar Single
	register_sidebar( array(
		'name' => __( 'Test Online', 'vi_theme' ),
		'id' => 'test-online',
		'before_widget' => '<div id="%1$s" class="%2$s blog-sidebar">',
		'after_widget' => '</div>',
		'before_title' => '<div class="blog-title"><h3>',
		'after_title' => '</h3></div>'
	));

	// Footer Block 1
	register_sidebar( array(
		'name' => __( 'Footer Block 1', 'vi_theme' ),
		'id' => 'footer-block-1',
		'before_widget' => '<div id="%1$s" class="%2$s menu_box_footer">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="title">',
        'after_title' => '</h3>'
	));
	// Footer Block 2
	register_sidebar( array(
		'name' => __( 'Footer Block 2', 'vi_theme' ),
		'id' => 'footer-block-2',
		'before_widget' => '<div id="%1$s" class="%2$s menu_box_footer">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="title">',
        'after_title' => '</h3>'
	));
	// Footer Block 3
	register_sidebar( array(
		'name' => __( 'Footer Block 3', 'vi_theme' ),
		'id' => 'footer-block-3',
		'before_widget' => '<div id="%1$s" class="%2$s menu_box_footer">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="title">',
        'after_title' => '</h3>'
	));
	// Footer Block 4
	register_sidebar( array(
		'name' => __( 'Footer Block 4', 'vi_theme' ),
		'id' => 'footer-block-4',
		'before_widget' => '<div id="%1$s" class="%2$s socail_footer menu_box_footer">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="title">',
        'after_title' => '</h3>'
	));
}
add_action( 'widgets_init', 'eweb_widgets_init' );
?>