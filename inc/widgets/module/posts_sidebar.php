<?php
add_action('widgets_init', 'vi_theme_posts_sidebar');
function vi_theme_posts_sidebar() {
        register_widget('vi_theme_posts_sidebar');
}
class vi_theme_posts_sidebar extends WP_Widget {
	/*-----------------------------------------------------------------------------------*/
	/*	Widget Setup
	/*-----------------------------------------------------------------------------------*/
    function __construct() {
        $widget_ops = array(
            'classname' => '',
            'description' => 'Bài viết trên Sidebar'
        );
        parent::__construct('vi_theme_posts_sidebar', 'Bài viết trên Sidebar', $widget_ops);
    }

	/*-----------------------------------------------------------------------------------*/
	/*	Display Widget
	/*-----------------------------------------------------------------------------------*/

    function widget($args, $instance) {
        extract($args);
        global $post;
        $title = apply_filters('widget_title', $instance['title']);
		$number = $instance['number'];
		$cat = $instance['cats'];

		foreach ($cat as $cat_id) {
			$args = array(
				'showposts'=> $number,
				'cat' => $cat_id,
				'order'=>'DESC',
				'featured' => 'yes'
			);
		}

        $args = array(
			'showposts'=> $number,
			'order'=>'DESC',
		);
		echo $before_widget;?>
		<?php echo $before_title;?><?php echo $title;?><?php echo $after_title;?>
			<ul>
				<?php $my_query = new wp_query($args);
				while($my_query->have_posts()):$my_query->the_post(); ?>
					<li class="clearfix">
						<a href="<?php the_permalink();?>" title="<?php the_title();?>">
							<?php the_post_thumbnail('thumbnail');?>
							<span><?php the_title();?></span>
							<p class="entry-meta">
							<span>
								<i class="fa fa-clock-o"></i> <?php _e('Cập nhật' , 'vi_theme'); ?>
								<label class="entry-date">
									<?php
										printf( __( '<span class="date-published">%1$s', 'vi_theme' ),
											get_the_date('d/m/Y') );
									?>
								</label>
							</span>
							</p>
						</a>
					</li>
				<?php endwhile; wp_reset_query();?>
			</ul>
        <?php echo $after_widget; //End Box
	}

	/*-----------------------------------------------------------------------------------*/
	/*	Update Widget
	/*-----------------------------------------------------------------------------------*/
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = $new_instance['number'];
		$instance['cats'] = $new_instance['cats'];

		return $instance;
	}
	/*-----------------------------------------------------------------------------------*/
	/*	Widget Settings (Displays the widget settings controls on the widget panel)
	/*-----------------------------------------------------------------------------------*/

	function form($instance) {
		$title = isset($instance['title']) ? esc_attr($instance['title']) : '';
		$number = isset($instance['number']) ? absint($instance['number']) : 6;
		?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>">Title</label></br>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>

		<p><label for="<?php echo $this->get_field_id('number'); ?>">Number Post</label>
			<input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>

		<p>
			<label for="<?php echo $this->get_field_id('cats'); ?>"><?php _e('Lựa chọn bài viết theo chuyên mục', 'vi_theme'); ?>
				<?php
				$categories = get_categories();
				echo "<br/>";
				foreach ($categories as $cat) {
					$option = '<input type="radio" id="' . $this->get_field_id('cats') . '[]" name="' . $this->get_field_name('cats') . '[]"';
					if (isset($instance['cats'])) {
						foreach ($instance['cats'] as $cats) {
							if ($cats == $cat->term_id) {
								$option = $option . ' checked="checked"';
							}
						}
					}
					$option .= ' value="' . $cat->term_id . '" />';
					$option .= '&nbsp;';
					$option .= $cat->cat_name;
					$option .= '&nbsp;('.$cat->count.' bài viết)';
					$option .= '<br />';
					echo $option;
				}
				?>
			</label>
		</p>
		<?php
	}
}
?>