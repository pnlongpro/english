<?php
add_action('widgets_init', 'vi_theme_post_by_id');
function vi_theme_post_by_id() {
        register_widget('vi_theme_post_by_id');
}
class vi_theme_post_by_id extends WP_Widget {
	/*-----------------------------------------------------------------------------------*/
	/*	Widget Setup
	/*-----------------------------------------------------------------------------------*/
    function __construct() {
        $widget_ops = array(
            'classname' => '',
            'description' => 'Bài viết theo ID (Sidebar)'
        );
        parent::__construct('vi_theme_post_by_id', '[EW] Bài viết theo ID (Sidebar)', $widget_ops);
    }

	/*-----------------------------------------------------------------------------------*/
	/*	Display Widget
	/*-----------------------------------------------------------------------------------*/

    function widget($args, $instance) {
        extract($args);
        global $post;
        $title = apply_filters('widget_title', $instance['title']);
		$postids = $instance['postids'];
		$argsid = explode(",", $postids);
        $args = array(
			'post__in' => $argsid
		);
		echo $before_widget;?>
		<?php echo $before_title;?><?php echo $title;?><?php echo $after_title;?>
		<ul>
			<?php
			$my_query = new wp_query($args);
			while($my_query->have_posts()):$my_query->the_post();?>
				<li class="clearfix">
					<a href="<?php the_permalink();?>" title="<?php the_title();?>">
						<?php the_post_thumbnail('thumbnail');?>
						<span><?php the_title();?></span>
						<p class="entry-meta">
							<span>
								<i class="fa fa-clock-o"></i> <?php _e('Cập nhật' , 'vi_theme'); ?>
								<label class="entry-date">
									<?php
										printf( __( '<span class="date-published">%1$s', 'vi_theme' ),
											get_the_date('d/m/Y') );
									?>
								</label>
							</span>
						</p>
					</a>
				</li>
			<?php endwhile; wp_reset_query();?>
		</ul>
        <?php echo $after_widget; //End Box
	}

	/*-----------------------------------------------------------------------------------*/
	/*	Update Widget
	/*-----------------------------------------------------------------------------------*/
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['postids'] = strip_tags($new_instance['postids']);

		return $instance;
	}
	/*-----------------------------------------------------------------------------------*/
	/*	Widget Settings (Displays the widget settings controls on the widget panel)
	/*-----------------------------------------------------------------------------------*/

	function form($instance) {
		$title = isset($instance['title']) ? esc_attr($instance['title']) : '';
		$postids = isset($instance['postids']) ? esc_attr($instance['postids']) : '';
		?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>">Title</label></br>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>

		<p><label for="<?php echo $this->get_field_id('postids'); ?>">Nhập ID các post</label>
			<input class="widefat" id="<?php echo $this->get_field_id('postids'); ?>" name="<?php echo $this->get_field_name('postids'); ?>" type="text" value="<?php echo $postids; ?>" /></p>
		<?php
	}
}
?>