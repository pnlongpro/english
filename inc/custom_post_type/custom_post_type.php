<?php
function vi_theme_custom_init() {

	// Khóa học
	$labels = array(
		'name' => 'Bài học',
		'singular_name' => 'All Posts',
		'add_new' => 'Add New',
		'add_new_item' => 'Add New Post',
		'edit_item' => 'Edit Post',
		'new_item' => 'New Post',
		'all_items' => 'All Post',
		'view_item' => 'View Post',
		'search_items' => 'Search Posts',
		'not_found' =>  'No posts found',
		'not_found_in_trash' => 'No posts found in Trash',
		'parent_item_colon' => '',
		'menu_name' => 'Bài học'
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'bai-hoc' ),
		'capability_type' => 'post',
		'has_archive' => false,
		'hierarchical' => false,
		//'menu_icon' => get_stylesheet_directory_uri() . '/images/icon-news.png',
		'menu_position' => 5,
		'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt'),
		'taxonomies' =>array('','post_tag')
	);
	register_post_type( 'bai-hoc', $args );

	// Thư viện
	$labels = array(
		'name' => 'Thư viện',
		'singular_name' => 'All Posts',
		'add_new' => 'Add New',
		'add_new_item' => 'Add New Post',
		'edit_item' => 'Edit Post',
		'new_item' => 'New Post',
		'all_items' => 'All Post',
		'view_item' => 'View Post',
		'search_items' => 'Search Posts',
		'not_found' =>  'No posts found',
		'not_found_in_trash' => 'No posts found in Trash',
		'parent_item_colon' => '',
		'menu_name' => 'Thư viện'
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'thu-vien' ),
		'capability_type' => 'post',
		'has_archive' => false,
		'hierarchical' => false,
		//'menu_icon' => get_stylesheet_directory_uri() . '/images/icon-news.png',
		'menu_position' => 5,
		'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt'),
		'taxonomies' =>array('','post_tag')
	);
	register_post_type( 'thu-vien', $args );

	// Cảm nhận học viên
	$labels = array(
		'name' => 'Cảm nhận học viên',
		'singular_name' => 'All Posts',
		'add_new' => 'Add New',
		'add_new_item' => 'Add New Post',
		'edit_item' => 'Edit Post',
		'new_item' => 'New Post',
		'all_items' => 'All Post',
		'view_item' => 'View Post',
		'search_items' => 'Search Posts',
		'not_found' =>  'No posts found',
		'not_found_in_trash' => 'No posts found in Trash',
		'parent_item_colon' => '',
		'menu_name' => 'Cảm nhận học viên'
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'cam-nhan' ),
		'capability_type' => 'post',
		'has_archive' => false,
		'hierarchical' => false,
		//'menu_icon' => get_stylesheet_directory_uri() . '/images/icon-news.png',
		'menu_position' => 5,
		'supports' => array( 'title', 'editor', 'thumbnail'),
		'taxonomies' =>array('','post_tag')
	);
	register_post_type( 'cam-nhan', $args );

	flush_rewrite_rules();
}
add_action( 'init', 'vi_theme_custom_init' );
?>