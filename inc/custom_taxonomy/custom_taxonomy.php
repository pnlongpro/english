<?php
	add_action( 'init', 'create_khoa_hoc_taxonomies', 0 );

	function create_khoa_hoc_taxonomies() {

		//==========Khóa học=====================
		$labels = array(
			'name'              => __( 'khóa học' ),
			'singular_name'     => __( 'Categories' ),
			'search_items'      => __( 'Search' ),
			'all_items'         => __( 'All Categories' ),
			'parent_item'       => __( 'Parent Category' ),
			'parent_item_colon' => __( 'Parent Category :' ),
			'edit_item'         => __( 'Edit Category' ),
			'update_item'       => __( 'Update Category' ),
			'add_new_item'      => __( 'Add New Category' ),
			'new_item_name'     => __( 'New Category Name' ),
			'menu_name'         => __( 'khóa học' ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'khoa-hoc' ),
		);

		register_taxonomy( 'khoa-hoc', array('bai-hoc'), $args );

		flush_rewrite_rules();
	}

?>