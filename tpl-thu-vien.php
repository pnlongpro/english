<?php
/**
 * Template Name: Thư viện
 */
get_header();
?>
<div class="container">
	<div class="row">
		<main id="main-content" class="main-content col-md-9">
			<h2 class="khoa-hoc-title"><?php the_title(); ?></h2>
			<?php
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			$args = array(
				'post_type' => 'thu-vien',
				'post_status' => 'publish',
				'posts_per_page' => 6,
				'order' => 'DESC',
				'paged' => $paged
			);
			$my_query = new wp_query($args);
			if($my_query->have_posts()) { ?>
				<div class="row">
					<ul class="library-list">
						<?php
						while ($my_query->have_posts()):$my_query->the_post(); ?>
							<li class="library-item col-md-4 col-sm-4 col-xs-6">
								<div class="library-content">
									<a class="thumbnail" href="<?php the_permalink();?>">
										<?php the_post_thumbnail('thu_vien_thumb_300x350');?>
									</a>
									<h3 class="library-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
								</div>
							</li>
						<?php endwhile;
						?>
					</ul>
				</div>

				<?php
                    wp_pagenavi( array( 'query' => $my_query ) );
                    wp_reset_query();
                ?>
			<?php } ?>
		</main><!-- #main-content -->
		<div id="sidebar-right" class="sidebar col-md-3">
			<?php if ( ! dynamic_sidebar( 'thu-vien' ) ) : ?>
			<?php endif;?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
